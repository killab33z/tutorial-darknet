# tutorial-darknet

NOTICE: the intent for this scripts are **not** to automatize but to **teach**
how to install a darknet server with i2p and possible TOR routers to vpn into from
external places. This is why **the script has no bash coding logic like if/for loops, file checks etc,
but is all line by line commands with out any logic/checks so students can follow step by step or 
even copy and paste manually**

Check at the end for a digital ocean **10$ coupon**

Video that goes with this tutorial(In Spanish) https://video.hispagatos.org/videos/watch/fc5fecba-b8a3-41c3-a2a4-d1757b35f1c8

1. Create **Ubuntu server 18.04 x64** on **DigitalOcean** or AWS/RackSpace etc..
   This tutorial uses **DigitalOcean**.

1. Get your new IP and ssh into the server as root@${ip}
   ```
   ssh root@${YOURIP}
   ```

1. Clone this repo
   ```
   git clone https://gitlab.com/rek2/tutorial-darknet.git
   ```
   
1. Run 
   ```
    cd ~/tutorial-darknet
    ./packages.sh
    ```

1. EDIT the vars file you should have in this repo
   you only need to change settings in one place:
   look for:
   ```
     export KEY_COUNTRY="US"
     export KEY_PROVINCE="MA"
     export KEY_CITY="Boston"
     export KEY_ORG="BinaryFreedom"
     export KEY_EMAIL="rek2@binaryfreedom.info"
     export KEY_OU="OperationsLab"
   ```
   change to your own info... 
   you will still be asked in **Step 7** for this info and other info besides.. when this happens just PRESS ENTER
   also you will be asked to add a password or not, to the cert created. up to you..
   if you do, everytime you add a user you have to add it..
   I recommend to learn just press "ENTER" to everything

1. Here is the example of when it ask for password and to sign the certificate:
   ```
   Please enter the following 'extra' attributes
   to be sent with your certificate request
   A challenge password []:
   An optional company name []:
   Using configuration from /etc/openvpn/easy-rsa/openssl-1.0.0.cnf
   Check that the request matches the signature
   Signature ok
   The Subject's Distinguished Name is as follows
   countryName           :PRINTABLE:'US'
   stateOrProvinceName   :PRINTABLE:'MA'
   localityName          :PRINTABLE:'Boston'
   organizationName      :PRINTABLE:'BinaryFreedom'
   organizationalUnitName:PRINTABLE:'OperationsLab'
   commonName            :PRINTABLE:'server'
   name                  :PRINTABLE:'server'
   emailAddress          :IA5STRING:'rek2@binaryfreedom.info'
   Certificate is to be certified until May 23 00:54:01 2027 GMT (3650 days)
   Sign the certificate? [y/n]:Y
   1 out of 1 certificate requests certified, commit? [y/n]y
   ```

1. You will see something similar above by running:
   ```
   cd ~/tutorial-darknet
   ./install-vpn.sh
   ```

1. Now lets add a user to the vpn **you can reuse this script going forward to add friends or more accounts**
   run and edit client.ovpn since this is the client file that will go to your computer/laptop as client.
   ```
   vim /etc/openvpn/client.ovpn #change "remote XX.XX.XX.XX 1194" to your openvpn server, IP 1194
   ```
   
1. Next step you have most of it configured so **NOTE** you have to press enter to continue and
   there are **2** prompts to enter Y to sign the certificate with your CA and add a passwd if you like.
   ```
   cd ~/tutorial-darknet
   source /etc/openvpn/easy-rsa/vars
   ./create-user-openvpn.sh  name_of_user_account 
   ```

1. Now you should have a accountname.tar.gz on your local folder. This is what you need to copy to your computer.
   In your computer **install openvpn** and locally run:
   ```
   scp root@XX.XX.XX.XX:nameofuseraccount.tar.gz .
   sudo tar -zxvf nameofuseraccount.tar.gz -C /etc/openvpn/   #this is an example for Ubuntu
   sudo tar -zxvf nameofuseraccount.tar.gz -C /etc/openvpn/client  #for Arch linux like Blackarch
   sudo mv /etc/openvpn/nameofuseraccount.ovpn /etc/nameofuseraccount.conf  #as example but RTFM for your distro
   sudo systemctl start openvpn  #Ubuntu example
   ```
   now RTFM read the documentation in running openvpn client in your laptop/computer..

1. edit firewall.sh
   and change the line with port 22 and add your home IP so is not open to everyone
   **NOTE** if you change this to a diff ip to from you are ssh into the box you will get kick out!!!
   ```
   vim firewall.sh
   ./firewall.sh
   ufw enable
   ```

1. now we can go ahead and install i2pd
   ```
   cd ~/tutorial-darknet
   ./install-i2p.sh
   ```
   
1. and thats it for base install
   run a netstat to check everything that is a i2p server is running on the openvpn interface 10.8.0.1
   ```
   netstat -ln
   ```

1. configure **weechat** for irc on the server so you can vpn or ssh into it. and chat from there if dont want to use local client
   you have to change in all the file the entry "hispauser"
   ```
   cd ~/tutorial-darknet
   sed -i s/hispauser/whatevernick/g irc.conf
   ./install-weechat.sh
   ```
1. Ok that's it. I recommend you run weechat under "screen" or similar program.
   That's it. you can run now weechat and it will auto connect to 10.8.0.1 port 6668
   it will auto connect to some channels like #hispagatos
   it may take a while since your i2p server is only running 1-2 hours or less.. Here's a [link](https://weechat.org/files/doc/devel/weechat_quickstart.en.html) to help with Weechat too.


Digital Ocean 10$ credit coupon!!!

Easily deploy an SSD cloud server on @DigitalOcean in 55 seconds. Sign up using my [link](https://m.do.co/t/96e575af1f05) and receive $10 in credit: https://m.do.co/t/96e575af1f05

```
Follow us here:

https://hispagatos.space/@hacktivists
https://hispagatos.space/@rek2
https://video.hispagatos.org/
https://hispagatos.org/
https://keybase.io/rek2
https://hispagatos.org/blog/2019-03-27-como-conectar-al-servidor-matrix-de-hispagatos-con-riot/  
```
